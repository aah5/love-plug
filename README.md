# love-plug

This extension makes it easier to connect to lovense toys

It allows setting up the toy ip one time and then keep it
fixed. This way the toy can be used with multiple websites
with ease.

## API

This extension extends javascript with special API to
control the toy.

In addition to that, it catches requests to `lovense.club`
and instead forwards them directly to local toy.

This allows using toys without internet connection or in
environments with strict DNS settings.
