browser.alarms.create({periodInMinutes: 0.03})

browser.alarms.onAlarm.addListener(async function() {
  await connection_heartbeat()
});

async function connection_heartbeat() {
  toys = await get_toys()

  await browser.storage.local.set({toys: toys})
}
