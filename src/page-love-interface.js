window.addEventListener("message", (event) => {
  if (event.source == window &&
    event.data &&
    event.data.direction == "from-page-script") {
    //console.log("Content script received message: \"" + event.data.message + "\"");
    let msg = event.data.message
    switch (msg.command) {
      case "vibrate":
        vibrate(msg.strength, msg.time)
        break;
      default:
        console.log("Unrecognized command: " + msg.command)
    }
  }
})
