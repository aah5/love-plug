document.body.onload = handle_storage_changes

browser.storage.onChanged.addListener(async (changes, area) => {
  if (area == 'local') {
    await handle_storage_changes()
  }
})

function make_header(icon, text) {
  return "<div class=\"icon-section-header\">"+
    "<span class=\"material-icons\">"+
      icon+
    "</span>"+
    "</div>"+
    "<div class=\"text-section-header\">"+
      text+
    "</div>"
}

function panel_section_wrapper(inner) {
  return "<div class=\"panel-section panel-section-header\"><div>"+
    inner+
    "</div></div>"
}

function make_toys_info(storage) {
  if ('toys' in storage && storage.toys.size > 0) {
    let list = ""
    storage.toys.forEach((v,k) => {
      list = list +
        `<tr class="row" id="device-${k}">`+
        `<td>${v.name}</td><td style="float:right">${v.battery}%</td>`+
        "</tr>"
    })

    let toys = `<div class="title">Toys:</div><br>`+
      "<table class=\"list\">"+
      "<tr><th>Name</th><th>Power</th></tr>"+
        list +
      "</table>"

    return panel_section_wrapper(toys + "<a href=\"https://www.lovense.com/r/m8tcfk\">Get More!</a>")
  } else {
    return panel_section_wrapper("Toys: None found, go <a href=\"https://www.lovense.com/r/m8tcfk\">get some!</a>")
  }
}

function update_connection_info(storage) {
  if ('ip' in storage) {
    document.getElementById("ip").value = storage.ip
  }
  if ('port' in storage) {
    document.getElementById("port").value = storage.port
  }
  if ('connected' in storage && storage.connected) {
    document.getElementById("connection-indicator").innerHTML = make_header("signal_wifi_on", "Connected");
    document.getElementById("connected-toys").innerHTML = make_toys_info(storage);
  } else {
    document.getElementById("connection-indicator").innerHTML = make_header("signal_wifi_off", "Not Connected");
    document.getElementById("connected-toys").innerHTML = "";
  }
}

async function handle_storage_changes() {
  storage = await browser.storage.local.get()
  update_connection_info(storage)
}

function update_conn() {
  browser.storage.local.set({
    ip: document.getElementById("ip").value,
    port: document.getElementById("port").value,
  })
}

document.getElementById("ip").oninput = update_conn
document.getElementById("port").oninput = update_conn
