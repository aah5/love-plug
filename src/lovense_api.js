async function get_endpoint_address() {
  storage = await browser.storage.local.get()

  ip = storage.ip
  port = storage.port

  return `http://${ip}:${port}/command`
}

async function send_request(request) {
  address = await get_endpoint_address()
  data = await fetch(address, {
    method: 'POST',
    body: JSON.stringify({...request, ...{apiVer: 1}}),
    headers: {'Content-Type': 'application/json'}
  }).catch(async (error) => {
    await browser.storage.local.set({connected: false})
  })

  if (data) {
    data_json = await data.json()
    if (data_json.code == 200 && data_json.type == "OK") {
      await browser.storage.local.set({connected: true})
    }
    if ('data' in data_json) {
      return data_json.data
    } else {
      return {}
    }
  } else {
    return {}
  }
}

async function get_toys() {
  data = await send_request({command: "GetToys"})

  if ('toys' in data) {
    return new Map(Object.entries(JSON.parse(data.toys)))
  } else {
    return {}
  }
}

async function vibrate(strength, time) {
  request = {command: "Function", action: "Vibrate:"+strength, timeSec: time}
  console.log(strength)
  return await send_request(request)
}
